/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformasi;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
/**
 *
 * @author laure
 */
public class Contoh extends JPanel {
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        latihanGambar(g);
    }
    public void latihanGambar(Graphics g){
        Graphics2D g2 = (Graphics2D)g;
        Ellipse2D.Float ling1 = new Ellipse2D.Float(100,100,200,200);
        Rectangle2D.Float rect1 = new Rectangle2D.Float(150,150,100,100);
        g2.rotate(Math.PI/10.0);
         Area donat = new Area (ling1);
         donat.subtract(new Area(rect1));
         
         g2.setColor(Color.blue);
         g2.fill(donat);
    }
}
