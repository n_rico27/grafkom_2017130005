/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animasi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.util.Random;
import javax.swing.JPanel;
import javax.swing.Timer;
/**
 *
 * @author laure
 */
public class Animasiny extends JPanel implements ActionListener, MouseListener,MouseMotionListener {
private Ellipse2D.Float[] bidak = new Ellipse2D.Float[10];
    private int x[]= new int[10],y[]=new int [10];
    private final static int SIZE=30;
    private Timer timer;
    private int mouseX, mouseY;
    private int arahX[] = new int [10], arahY[] = new int [10];
    private final static int SPEED=2;
    private int arah[] = new int[10];
    private boolean cek[] = new boolean[10];
    
public Animasiny (){
Random rnd = new Random();
              
        for(int i=0;i<10;i++){
            x[i] = 80 ;
            y[i] = 100+i*50;
            cek[i] = false;
            this.arahX[i] = 1;
            this.arahY[i] = 1;
            this.bidak[i] = new Ellipse2D.Float(x[i], y[i], SIZE, SIZE);//(x*SIZE, y*SIZE, SIZE,SIZE);
        }
            
        this.timer=new Timer (33,this);//(500,this);
        this.timer.start();
        
        addMouseListener(this);
        addMouseMotionListener(this);
    
}
        public void paintComponent (Graphics g){
        super.paintComponent(g);
          for(int i=0;i<10;i++){    
                
            Graphics2D g2 = (Graphics2D) g;
            if(x[i]<=mouseX && x[i]+SIZE > mouseX && y[i]<=mouseY && y[i]+SIZE>=mouseY){  //possi bulat utk diganti jadi warna kuning
                cek[i] = true;
            }
            if(cek[i])g2.setColor(Color.red);
            else g2.setColor(Color.black);
            g2.fill(bidak[i]);
          } 
        }
          @Override
          public void mouseMoved(MouseEvent e){
              
        }
          public void actionPerformed(ActionEvent e){
        for (int i = 0; i < 10; i++) {
            if(arah[i]==0)
            this.x[i] += (SPEED*arahX[i]);
            else 
            this.y[i] += (SPEED*arahY[i]);
            if(this.x[i] < 0 || this.x[i] >getWidth())
                arahX[i] *= -1;
            if(this.y[i] < 0 || this.y[i] >getHeight())
                arahY[i] *= -1;
            this.bidak[i].setFrame(x[i], y[i], SIZE,SIZE);
        }
            
//        this.x++;
//        this.bidak.setFrame(x*SIZE, y*SIZE, SIZE,SIZE);
        repaint();
    }
    
     public void mouseClicked(MouseEvent e){
         
        mouseX = e.getX(); //mengcopy posisi x
        mouseY = e.getY(); //mengcopy posisi y
         for(int i=0;i<10;i++){
             if(x[i]<=mouseX && x[i]+SIZE > mouseX && y[i]<=mouseY && y[i]+SIZE>=mouseY && arah[i]==0)
                arah[i]=1;
             else if(x[i]<=mouseX && x[i]+SIZE > mouseX && y[i]<=mouseY && y[i]+SIZE>=mouseY && arah[i]==1)
                arah[i]=0;
         }
         
    }
    public void mouseEntered(MouseEvent e){
        
    }
    public void mouseExited(MouseEvent e){}
    public void mousePressed(MouseEvent e){}
    public void mouseReleased(MouseEvent e){}
    public void mouseDragged(MouseEvent e) {}
}


