/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobajframe;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
/**
 *
 * @author Multimedia
 */
public class CobaPanel extends JPanel {
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.drawString("ini tulisan dalam panel", 200, 200);
       // g.setColor(Color.BLACK);
        KotakWarnaSolid(g, 50, 50, 100, 100);//graphics,x,y,lbr,tinggi
        KotakWarnaGradasi(g, 200, 200, 200, 200);//graphics,x,y,lbr,tinggi
        Kotak2Warna(g, 100, 100, 200, 150, 
                new Color(200,50,150), new Color(0,255,150));
        //persegi
//        g.drawLine(60, 60, 60, 150);
//        g.drawLine(60, 60, 140, 60);
//        g.drawLine(140, 60, 140, 150);
//        g.drawLine(60, 150, 140, 150);
        
        //bintang
//        g.drawLine(200, 300, 300, 300);
//        g.drawLine(200, 300, 275, 350);
//        g.drawLine(250, 275, 275, 350);
//        g.drawLine(250, 275, 225, 350);
//        g.drawLine(300, 300, 225, 350);
        
      
        
    }
    public void KotakWarnaSolid(Graphics g, int x, int y, int lebar, int tinggi){
        g.setColor(new Color (100,100,100));
          for(int i=0; i<lebar; i++){
               g.drawLine(x+i, y, x+i, y+ tinggi);
//        for(int i=0; i<tinggi; i++){
//            g.drawLine(x, y+i, x+lebar, y+i);
        }
    }
    public void KotakWarnaGradasi(Graphics g, int x, int y, int lebar, int tinggi){
        double step = 128.0/lebar; // intensitas warna, semaikn besar semakin bnyak putihnya
        double dgs = 0;
        int ic;
        for(int i=0; i<lebar; i++){
            ic = (int)Math.round(dgs);
            g.setColor(new Color(ic,ic,ic));
            g.drawLine(x+i, y, x+i, y+ tinggi);
            dgs = dgs + step;
            //gradasi warna dari hitam ke abu (horizontal)
            
//        double step = 128.0/tinggi; // intensitas warna, semaikn besar semakin bnyak putihnya
//        double dgs = 0;
//        int ic;
//        for(int i=0; i<tinggi; i++){
//            ic = (int)Math.round(dgs);
//            g.setColor(new Color(ic,ic,ic));
//            g.drawLine(x, y+i, x+lebar, y+i);
//            dgs = dgs + step;
        }
    }
public void Kotak2Warna(Graphics g, int x, int y, int lebar, int tinggi,Color start, Color end){
    double step = 256.0/lebar; // intensitas warna, semaikn besar semakin bnyak putihnya
        int R=(end.getRed()- start.getRed());  
        int G=(end.getGreen()- start.getGreen());
        int B=(end.getBlue()- start.getBlue());
       
        double stepR = R / lebar;
        double stepG = G/ lebar;
        double stepB = B/ lebar;
        
        int dcR = start.getRed(); 
        int dcG = start.getGreen();
        int dcB = start.getBlue(); 
       // System.out.println(stepR);
        //System.out.println(stepG);
        //System.out.println(stepB);
        
        for (int i=0; i<lebar; i++){  
           g.setColor(new Color(dcR, dcG, dcB));
           g.drawLine(x+i, y, x+i, y+lebar);
           dcR = dcR + (int) stepR;
           dcG = dcG + (int) stepG;
           dcB = dcB + (int) stepB;
        }
         
        }  
}

