/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobajframe;
import javax.swing.SwingUtilities;
import javax.swing.JFrame;
/**
 *
 * @author Multimedia
 */
public class CobaJFrame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable(){
        public void run (){
            createAndShowGUI();
            
        }
    });
    }
   private static void createAndShowGUI(){
        JFrame f = new JFrame ("Swing Paint Demo");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(500,500);
        f.add(new CobaPanel());
        f.setVisible(true);
   } 
}
